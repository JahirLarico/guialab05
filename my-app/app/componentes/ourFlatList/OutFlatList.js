import React from "react";
import { View , FlatList, StyleSheet, Text, TouchableOpacity} from 'react-native'

const DATA = [
];

function Item({ title, showAlert }){
    return (
        <TouchableOpacity onPress={showAlert}>
            <View style={styles.item}>
                <Text style={styles.title}>{title}</Text>
            </View>
        </TouchableOpacity>
        
    );
}
const ListEmpty = () => {
    return (
        <View style={styles.MainContainer}>
            <Text style={{ textAlign: 'center'}}>No data found</Text>
        </View>
    );
};

const OurFlatList = props => {
    return (
        <View style={styles.container}>
        <FlatList
            data={DATA}
            renderItem= {({item}) => (
                <Item title={item.description} showAlert={props.showAlert}></Item>
            )}
            keyExtractor={item => item.id}
            ListEmptyComponent={ListEmpty}
        ></FlatList>
    </View>
    )
    
} 
/*
export default function OurFlatList(){
    return (
        <View style={styles.container}>
            <FlatList
                data={DATA}
                renderItem={({ item}) => <Item title={item.title}></Item>}
                keyExtractor={item => item.id}
            ></FlatList>
        </View>
    )
}
*/
const styles = StyleSheet.create({
    container : {
        flex: 1 ,
        marginTop: 20,
    },
    item: {
        backgroundColor: 'orange',
        padding: 20,
        marginVertical: 8,
        marginHorizontal:16,
    },
    title: {
        fontSize: 32,
    },
});

export default OurFlatList;