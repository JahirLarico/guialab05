import React, { Component } from 'react';
import { StyleSheet, Text, View , Image , TouchableOpacity, TextInput, Alert, Button} from 'react-native';
import Message from './app/componentes/message/Message';
import Body from './app/componentes/body/Body'
import OurFlatList from './app/componentes/ourFlatList/OutFlatList'
import ConexionFetch from './app/componentes/conexionFetch/ConexionFetch';

import {NavigationContainer} from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack'
const provincias = [
  {
    id: 1,
    nombre: 'Arequipa'
  },
  {
    id: 2,
    nombre: 'Puno'
  },
  {
    id: 3,
    nombre: 'Cuzco'
  }
]


function HomeScreen({navigation}){
  return(
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text> Home Screen</Text>

      <Button
        title='Go to Details'
        onPress={() => navigation.navigate('Details')}
      ></Button>
    </View>
  )
}

function DetailsScreen({navigation}){
  return(
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text> Detail Screen</Text>

      <Button
        title='Go bacl to first screen in stack'
        onPress={() => navigation.popToTop()}
      ></Button>
    </View>
  )
}
const Stack = createStackNavigator();
export default class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      textValue: '',
      count : 0,
    };
  }

  changeTextInput = text =>{
    console.log(text);

    this.setState({textValue: text});
  }

  onPress = () => {
    this.setState({
      count: this.state.count + 1
    });
  }
  showAlert = () => {
    Alert.alert(
      'Titulo',
      'Mensaje',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'Ok', onPress: () => console.log('Ok pressed')},
      ],
      {cancelable: false},
    );
  };

  
  render() {
    /*
    return (
      <View style={styles.container}>

        <OurFlatList showAlert={this.showAlert}></OurFlatList>
      </View>
    )
    */
    
   return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen}></Stack.Screen>
        <Stack.Screen name="Details" component={DetailsScreen}></Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
   )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal:10,
  },
  text:{
    alignItems: 'center',
    padding: 10,
  },
  button: {
    top: 10,
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
  countText: {
    color: '#FF00FF'
  }
});
